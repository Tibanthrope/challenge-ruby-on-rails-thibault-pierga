class Article < ApplicationRecord
	has_many :ratings, dependent: :destroy
	validates :title, presence: true, length: { minimum: 5 }
 	
end
