class CreateRatins < ActiveRecord::Migration[6.0]
  def change
    create_table :ratins do |t|
      t.string :user
      t.integer :score
      t.references :articles, null: false, foreign_key: true

      t.timestamps
    end
  end
end
