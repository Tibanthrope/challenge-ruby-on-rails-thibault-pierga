class RemoveBodyFromRating < ActiveRecord::Migration[6.0]
  def change

    remove_column :ratings, :body, :text
  end
end
